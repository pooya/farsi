---
layout: page
title: پروژه های انجام شده
excerpt: "پروژه های انجام شده توسط پویا گلچیان"
modified: 2016-01-19
image:
  feature: bg4.jpg
  <!--credit: OpenSource!-->
  <!--creditlink: -->
---
# وب سایت جدیدم  بر پایه جکیل گیت هاب
میتونید مثل این وب سایت رو داشته باشید یا من در توسعه این وب سایت کمک کنید [repo](https://github.com/pooya-golchian/pooya-golchian.github.io).
لینک وب سایت من [url](http://pooyagolchian.ir).

# عکس پرینت
برنامه نویس عکس پرینت [axprint.com](http://axprint.com). شهریور ۹۴ تا به الان.

## پروژه های من در عکس پرینت
* **ساخت و راه اندازی وبلاگ عکس پرینت با دروپال و ادمین سرور لینوکس** [blog.axprint.com](http://blog.axprint.com)
* **طراحی مجدد وب سایت که هنوز در جریان هست** [axprint.com](http://axprint.com)
* **ساخت لندینگ پیج ها و کمپین های عکس پرینت**
    - [axprint.com/landing/loveday94](http://axprint.com/landing/loveday94)  
    - [axprint.com/landing/pishgaman](http://axprint.com/landing/pishgaman)
    - [axprint.com/landing/coldseason](http://axprint.com/landing/coldseason)
    - [axprint.com/landing/fetr95](http://axprint.com/landing/fetr95)

# شرکت مهندسی نرم افزار رایورز
برنامه نویس دروپال و طراح و گرافیست وب [شرکت مهندسی نرم فزار رایورز](http://rayvarz.com) - به مدت یکسال.


## پروژه های من در رایورز
* **طراحی و ساخت پرتال مدیریت کسب و کار رایورز** [پرتال و پایگاه دانش BPM رایورز](http://bpms.rayvarz.com)
* **نگهداری از وب سایت و پرتال های دروپالی رایورز**
* **طراحی وکتور های گرافیکی با فوتوشاپ و ایلاستریتور**


# بازی دو بعدی ماهی پرنده
این بازی برای سیستم عامل های اندروید و ویندوز نوشته شده است. هم اکنون میتوانید این بازی رو از ایران اپس  دانلود کنید [دانلود از ایران اپس](http://iranapps.ir/app/com.pooya.FlyFish).
